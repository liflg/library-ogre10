Lwo2Mesh v0.89 by Dennis Verbeek (dennis.verbeek@chello.nl)

Lwo2Mesh is a commandline tool to convert lightwave objects into ogre-meshes. Use -? to get help. Use *.lwo to convert multiple objects in one run.

The lightwave-object loading code is based on Lightwave SDK code by Ernie Wright.

This program is distributed as-is and WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Dennis Verbeek

Things yet to be implemented:
- Exporting Skeletons (I've made a small start)
- Multiple texture layers
