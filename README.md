Website
=======
http://www.ogre3d.org/

License
=======
LGPL v2 (see the file source/COPYING)

Version
=======
1.0.7

Source
======
ogre-linux_osx-v1-0-7.tar.bz2 (sha256: e4dbd75feb877173f3cc9c83f10e4f5326affebc5833c4c0f49c349bfbda84af)

Requires
========
* CEGUI
* DevIL
* FreeType
* jpeg-turbo
* NVIDIA Cg Toolkit
* SDL1
* tiff
* zziplib
